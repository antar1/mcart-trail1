package cart_example.mcart_trail1;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cart
{
	private static final Logger logger = LogManager.getLogger(Cart.class);

	private ArrayList<Item> local_cart = new ArrayList<Item>();

	public int get_cart_quantity()
	{
		return this.local_cart.size();
	}

	public boolean view_cart()
	{
		if (local_cart.isEmpty())
		{
			return false;
		}
		else
		{
			for (Item i : local_cart)
				System.out.println(i.getName() + "\t" + i.getQuan());
			return true;
		}
	}

	public void add_to_cart(int id, int n) throws Exception
	{
		try
		{
			Item i = Database.query_s("Select * from items where id=" + id + ";");
			local_cart.add(new Item(i.getName(), n, i.getId()));
			i = null;

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean remove_from_cart(int id)
	{
		int in;
		for (in = 0; in < local_cart.size(); in++)
		{
			if (local_cart.get(in).getId() == id)
				break;
		}
		if (in == local_cart.size())
		{
			return false;
		}
		else
		{
			local_cart.remove(in);
			return true;
		}
	}

	public void checkout() throws Exception
	{
		char c;
		Scanner sc = Main.sc;
		c = sc.next().charAt(0);
		if (c == 'y' || c == 'Y')
			this.purchase();
	}

	public void purchase() throws Exception
	{
		String s = "ARRAY[";
		int j = 0;
		for (Item i : this.local_cart)
		{
			try
			{
				Database.query_t("update items set quantity=quantity-" + i.getQuan() + "where id=" + i.getId() + ";");
				if (j > 0)
					s = s + ",";
				s = s + "[" + i.getId() + "," + i.getQuan() + "]";
				j++;
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				logger.error("Checkout sql error\n" + e);
			} catch (Exception e)
			{
				logger.error("Checkout error" + e);
			}
		}

		Date date = new Date();
		Timestamp t = new Timestamp(date.getTime());

		Database.query_t("insert into invoice (time, items) values('" + t.toString() + "', " + s + "]);");

		System.out.println("Order placed!!");
		this.local_cart.clear();
	}

}
