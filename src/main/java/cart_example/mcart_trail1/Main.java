package cart_example.mcart_trail1;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main
{

	private static final Logger logger = LogManager.getLogger(Main.class);
	static Scanner sc = new Scanner(System.in);;

	public static void main(String args[])
	{
		int ch = -1, temp, temp1;
		Items items = new Items();
		Cart cart = new Cart();
		System.out.println("Welcome to localcart");
		// Items v = new Items();
		try
		{
			do
			{
				System.out.println("\n\n1.View Products\n2.View cart\n-1: Exit\n\nChoose an option:");
				ch = sc.nextInt();

				switch (ch)
				{
				case 1:

					do
					{
						items.list_item();
						System.out.println("\n\nTo select a product, enter product id\nTo go back, enter -2");
						ch = sc.nextInt();
						switch (ch)
						{
						case -2:
							break;

						default:
							if (ch == 0 || ch == -1 || ch < -2 || ch > items.max_id())
							{
								System.out.println("Invalid option\nEnter again!!");
								ch = -3;
								break;
							}
							temp1 = items.select_item(ch);
							temp = ch;
							if (temp1 == 1)
							{
								do
								{

									System.out.println("\n\nPress 1 for add to cart and -3 to go back");
									ch = sc.nextInt();
									switch (ch)
									{
									case 1:
										System.out.println("Enter the required quantity:");
										do
										{
											temp1 = sc.nextInt();
											if (temp1 < 1)
												System.out.println("Invalid number, enter again:");
										} while (temp1 < 1);

										cart.add_to_cart(temp, temp1);
										System.out.println("Item added to cart!!");

										break;

									case -3:
										break;

									default:
										System.out.println("Invalid number, enter again:");
									}
								} while (ch != -3);
							}
							else
							{

								do
								{
									System.out.println("press -3 to go back");
									ch = sc.nextInt();
									switch (ch)
									{
									case -3:
										break;
									default:
										System.out.println("Invalid number, enter again:");
									}
								} while (ch != -3);
							}
						}
					} while (ch != -2);
					break;

				case 2:
					cart.view_cart();
					if (cart.get_cart_quantity() > 0)
					{
						do
						{
							System.out.println("\nTo proceed checkout and place order, press 1\n"
									+ "To remove an item, press 2\nTo go back, press -2\n");
							ch = sc.nextInt();
							switch (ch)
							{
							case 1:
								cart.checkout();
								ch = -2;
								break;

							case 2:
								System.out.println("Enter item id: ");
								temp = sc.nextInt();
								if (cart.remove_from_cart(temp) == true)
									System.out.println("Item removed!");
								else
									System.out.println("Item not found or cant be remved!!");
								break;

							case -2:
								break;

							default:
								System.out.println("Invalid number, enter again:");

							}
						} while (ch != -2);

					}

				case -1:
					System.out.println("Thanks you!!");
					break;

				default:
					System.out.println("Invalid number, enter again:");
				}
			} while (ch != -1);

		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			sc.close();
		}
	}
}
