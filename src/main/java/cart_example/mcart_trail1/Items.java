package cart_example.mcart_trail1;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * 
 * 
 */
public class Items
{
	private ArrayList<Item> items;
	
	private static final Logger logger = LogManager.getLogger(Items.class);

	public int max_id()
	{
		return items.get(items.size()-1).getId();
	}
	public void list_item()
	{
		try
		{
			items = Database.query_m("select * from items where quantity > 0 order by id asc;");
		} catch (SQLException e)
		{
			logger.error("error"+e);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Id\tName\n");
		for (Item i : items)
			System.out.println(i.getId() + "\t" + i.getName());
	}

	public int select_item(int id) throws Exception
	{
		try
		{
			Item i = Database.query_s("Select * from items where id=" + id + ";");
			System.out.println("Name: " + i.getName());
			System.out.println("Availability: " + (i.getQuan() > 0 ? "Available" : "Not available"));
			if(i.getQuan()>0)
				return 1;
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return -3;
	}

}
