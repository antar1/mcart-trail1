package cart_example.mcart_trail1;

public class Item
{
	private String pr_name;
	private int pr_id;
	private int pr_quantity;

	public Item(String name, int quan, int id)
	{
		this.pr_name = name;
		this.pr_id = id;
		this.pr_quantity = quan;
	}

	public String getName()
	{
		return this.pr_name;
	}

	public int getId()
	{
		return this.pr_id;
	}

	public int getQuan()
	{
		return this.pr_quantity;
	}
}
