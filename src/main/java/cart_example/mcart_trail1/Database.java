package cart_example.mcart_trail1;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Database
{
	private static Statement stmt;
	private static Connection connection = null;
	private static ResultSet rs;

	private static final Logger logger = LogManager.getLogger(Database.class);

	private static void init_postgre() throws Exception
	{

		try
		{
			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e)
		{
			logger.error("Where is your PostgreSQL JDBC Driver? " + "Include inyour library path!");
			StringWriter sw=new StringWriter();
			PrintWriter pw=new PrintWriter(sw);
			e.printStackTrace(pw);
			logger.error(sw.toString());
			throw e;
		}

		try
		{
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/new1", "postgres", "good");
		} catch (SQLException e)
		{
			logger.error("Connection Failed! Check output console", e);
			throw e;
		}

		try
		{
			stmt = connection.createStatement();
		} catch (SQLException e)
		{
			logger.error("Failed to make query driver!", e);
			throw e;
		}

	}

	/*
	 * For select Statement Query.
	 */
	public static ArrayList<Item> query_m(String sql) throws Exception
	{
		ArrayList<Item> l = new ArrayList<Item>();
		try
		{
			init_postgre();
			logger.info("Database Connection Established.");
			logger.info("Query::" + sql);
			rs = stmt.executeQuery(sql);

			logger.info("Query executed!!");
			while (rs.next())
			{
				l.add(new Item(rs.getString("name"), rs.getInt("quantity"), rs.getInt("id")));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw e;
		} finally
		{
			if (rs.isClosed())
				rs.close();
			if (stmt.isClosed())
				stmt.close();
			if (connection.isClosed())
				connection.close();
		}
		return l;
	}

	public static Item query_s(String sql) throws Exception
	{
		Item l = null;
		try
		{
			init_postgre();
			logger.info("Database Connection Established.");
			logger.info("Query::" + sql);
			rs = stmt.executeQuery(sql);

			logger.info("Query executed!!");
			while (rs.next())
			{
				l = new Item(rs.getString("name"), rs.getInt("quantity"), rs.getInt("id"));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw e;
		} finally
		{
			if (rs.isClosed())
				rs.close();
			if (stmt.isClosed())
				stmt.close();
			if (connection.isClosed())
				connection.close();
		}
		return l;
	}

	/*
	 * For Insert, Update, Delete Query.
	 */
	public static int query_t(String sql) throws Exception
	{
		int i = 0;
		try
		{
			init_postgre();
			logger.info("Database Connection Established.");
			logger.info("Query::" + sql);
			i = stmt.executeUpdate(sql);
			logger.info("Query executed!!");
		} catch (SQLException e)
		{
			e.printStackTrace();
			throw e;
		} finally
		{
			if (rs.isClosed())
				rs.close();
			if (stmt.isClosed())
				stmt.close();
			if (connection.isClosed())
				connection.close();
		}

		return i;
	}

}
